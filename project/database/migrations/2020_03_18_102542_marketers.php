<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Marketers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketers', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->unsignedTinyInteger('division');
            $table->string('national_code',10);
            $table->string('photo',256);
            $table->string('national_card',256);
            $table->string('verify_photo',256);
            $table->boolean('state')->default(true);
            $table->timestamp('start_contract_date')->useCurrent();
            $table->timestamp('end_contract_date')->nullable();
            $table->timestamp('renewal_contract_date')->nullable();
            $table->string('contract_text');
            $table->string('bank_number',16);
            $table->string('Ibane',32);
            $table->unsignedInteger('user_id');
            $table->unsignedMediumInteger('admin_generated_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('admin_generated_id')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketers');
    }
}
