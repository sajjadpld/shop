<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductRepositories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_repositories', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->unsignedMediumInteger('count');
            $table->unsignedInteger('surplus_cost')->default(0);
            $table->unsignedInteger('product_attribute_id');
            $table->unsignedInteger('owner_id');
            $table->unsignedSmallInteger('repository_id');
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->foreign('product_attribute_id')->references('id')->on('product_attributes');
            $table->foreign('owner_id')->references('id')->on('users');
            $table->foreign('repository_id')->references('id')->on('repositories');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_repositories');
    }
}
