<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductSpecificationsValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_specifications_values', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string('value',128);
            $table->string('description',1024)->nullable();
            $table->unsignedSmallInteger('key_id');
            $table->unsignedMediumInteger('admin_id');
            $table->timestamps();

            $table->foreign('key_id')->references('id')->on('product_specifications_keys');
            $table->foreign('admin_id')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_specifications_values');
    }
}
