<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class PairProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pair_products', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->unsignedInteger('product_attribute_id');
            $table->unsignedInteger('pair_id');
            $table->unsignedMediumInteger('admin_id');
            $table->timestamps();

            $table->foreign('product_attribute_id')->references('id')->on('product_attributes');
            $table->foreign('pair_id')->references('id')->on('product_attributes');
            $table->foreign('admin_id')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pair_products');
    }
}
