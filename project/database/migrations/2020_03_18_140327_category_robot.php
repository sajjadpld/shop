<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CategoryRobot extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_robot', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('post_body',2048);
            $table->unsignedSmallInteger('category_id');
            $table->unsignedTinyInteger('robot_id')->nullable();
            $table->unsignedMediumInteger('admin_id');
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories');
            $table->foreign('robot_id')->references('id')->on('robots');
            $table->foreign('admin_id')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_robot');
    }
}
