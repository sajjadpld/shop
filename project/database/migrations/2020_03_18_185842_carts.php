<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Carts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->string('description',1024)->nullable();
            $table->string('user_description_get_cart',2048)->nullable();
            $table->boolean('state')->default(false);
            $table->unsignedInteger('surplus_cost')->default(0);
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('rebate_id')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('rebate_id')->references('id')->on('rebates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}
