<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductUserQuestionAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_user_question_answers', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->string('answer',2048);
            $table->unsignedInteger('question_id');
            $table->unsignedInteger('user_id');
            $table->timestamps();

            $table->foreign('question_id')->references('id')->on('product_user_questions');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_user_question_answers');
    }
}
