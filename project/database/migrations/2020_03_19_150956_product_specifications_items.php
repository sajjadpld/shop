<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductSpecificationsItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_specifications_items', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->unsignedSmallInteger('category_id');
            $table->unsignedSmallInteger('key_id');
            $table->unsignedMediumInteger('admin_id');
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('product_specifications_category');
            $table->foreign('key_id')->references('id')->on('product_specifications_keys');
            $table->foreign('admin_id')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_specifications_items');
    }
}
