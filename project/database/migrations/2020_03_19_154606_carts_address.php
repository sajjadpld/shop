<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CartsAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts_address', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->string('address',2048);
            $table->string('post_code',128);
            $table->string('latitude',128);
            $table->unsignedSmallInteger('province_id');
            $table->unsignedInteger('cart_id');
            $table->timestamps();

            $table->foreign('province_id')->references('id')->on('provinces');
            $table->foreign('cart_id')->references('id')->on('carts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts_address');
    }
}
