<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductSpecifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_specifications', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->boolean('is_for_brief_specifications')->default(false);
            $table->unsignedInteger('product_id');
            $table->unsignedMediumInteger('item_id');
            $table->unsignedMediumInteger('value_id');
            $table->unsignedMediumInteger('admin_id');
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('item_id')->references('id')->on('product_specifications_items');
            $table->foreign('value_id')->references('id')->on('product_specifications_values');
            $table->foreign('admin_id')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_specifications');
    }
}
