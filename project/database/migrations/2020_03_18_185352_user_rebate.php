<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserRebate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_rebate', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->boolean('used')->default(false);
            $table->unsignedInteger('rebate_id');
            $table->unsignedInteger('user_id');
            $table->unsignedMediumInteger('admin_id');
            $table->timestamps();

            $table->foreign('rebate_id')->references('id')->on('rebates');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('admin_id')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_rebate');
    }
}
