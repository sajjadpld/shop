<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->string('instagram_link',128)->nullable();
            $table->string('code',64);
            $table->string('persian_title',256);
            $table->string('english_title',256)->nullable();
            $table->string('photo',1024);
            $table->string('brief_description',1024)->nullable();
            $table->string('general_description',1024)->nullable();
            $table->unsignedInteger('cost');
            $table->unsignedInteger('buy');
            $table->unsignedInteger('visit')->default(0);
            $table->boolean('original')->default(false);
            $table->string('clip',256)->nullable();
            $table->unsignedSmallInteger('points');
            $table->boolean('state')->default(true);
            $table->unsignedInteger('user_id');
            $table->unsignedTinyInteger('gender_id');
            $table->unsignedSmallInteger('brand_id')->nullable();
            $table->unsignedSmallInteger('country_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('gender_id')->references('id')->on('genders');
            $table->foreign('brand_id')->references('id')->on('brands');
            $table->foreign('country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
