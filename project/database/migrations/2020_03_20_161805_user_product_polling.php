<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserProductPolling extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_product_polling', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->unsignedTinyInteger('value');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('product_polling_id');
            $table->unsignedMediumInteger('poll_choice_id');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('product_polling_id')->references('id')->on('product_polling');
            $table->foreign('poll_choice_id')->references('id')->on('poll_choices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_product_polling');
    }
}
