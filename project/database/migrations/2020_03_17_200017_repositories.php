<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Repositories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('repositories', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('latitude',128);
            $table->string('name',64);
            $table->string('address',1024);
            $table->string('phone',16);
            $table->boolean('state')->default(true);
            $table->unsignedInteger('owner_id');
            $table->unsignedSmallInteger('province_id');
            $table->unsignedMediumInteger('admin_generated_id')->nullable();
            $table->timestamps();

            $table->foreign('owner_id')->references('id')->on('users');
            $table->foreign('province_id')->references('id')->on('provinces');
            $table->foreign('admin_generated_id')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('repositories');
    }
}
