<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Admins extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->mediumIncrements('id');
            $table->string('national_code',10);
            $table->string('photo',256)->nullable();
            $table->boolean('state')->default(true);
            $table->string('bank_number',16)->nullable();
            $table->string('Ibane',32)->nullable();
            $table->unsignedInteger('user_id');
            $table->unsignedMediumInteger('admin_generated_id')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
