<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductAttributeList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_attribute_list', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->unsignedInteger('product_attribute_id');
            $table->unsignedTinyInteger('attribute_key_id')->nullable();
            $table->unsignedSmallInteger('attribute_value_id')->nullable();
            $table->timestamps();

            $table->foreign('product_attribute_id')->references('id')->on('product_attributes');
            $table->foreign('attribute_key_id')->references('id')->on('attribute_keys');
            $table->foreign('attribute_value_id')->references('id')->on('attribute_values');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_attribute_list');
    }
}
