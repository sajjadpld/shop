<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CartProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_product', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->unsignedMediumInteger('count')->default(1);
            $table->unsignedInteger('cart_id');
            $table->unsignedInteger('product_repository_id');
            $table->unsignedInteger('rebate_id');
            $table->unsignedMediumInteger('seller_id');
            $table->timestamps();

            $table->foreign('cart_id')->references('id')->on('carts');
            $table->foreign('product_repository_id')->references('id')->on('product_repositories');
            $table->foreign('rebate_id')->references('id')->on('rebates');
            $table->foreign('seller_id')->references('id')->on('sellers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_product');
    }
}
