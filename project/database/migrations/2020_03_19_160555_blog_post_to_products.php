<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BlogPostToProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_post_to_products', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->unsignedInteger('post_id');
            $table->unsignedInteger('product_id');
            $table->unsignedMediumInteger('admin_id');
            $table->timestamps();

            $table->foreign('post_id')->references('id')->on('blog_posts');
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('admin_id')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_post_to_products');
    }
}
