<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ProductDisadvantagesAdvantages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_disadvantages_advantages', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->string('title',24);
            $table->boolean('is_advantage');
            $table->unsignedInteger('product_id');
            $table->unsignedMediumInteger('admin_id');
            $table->timestamps();

            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('admin_id')->references('id')->on('admins');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_disadvantages_advantages');
    }
}
