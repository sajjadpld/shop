
<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from www.radixtouch.in/templates/admin/lorax/source/rtl/ by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 22 Mar 2020 08:26:04 GMT -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <title>Lorax - Bootstrap 4 Admin Dashboard Template</title>
    <!-- Favicon-->
    <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon">
    <!-- Plugins Core Css -->
    <link href="assets/css/app.min.css" rel="stylesheet">
    <link href="assets/js/bundles/materialize-rtl/materialize-rtl.min.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="assets/css/style.css" rel="stylesheet" />
    <!-- Theme style. You can choose a theme from css/themes instead of get all themes -->
    <link href="assets/css/styles/all-themes.css" rel="stylesheet" />
</head>
<body class="light rtl">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30">
            <img class="loading-img-spin" src="assets/images/loading.png" width="20" height="20" alt="admin">
        </div>
        <p>Please wait...</p>
    </div>
</div>
<!-- #END# Page Loader -->
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
    @include('panel.sidebars.sidebar_top')
<div>
    @include('panel.sidebars.sidebar_menu')
    @include('panel.sidebars.sidebar_setting')

</div>

@yield('content')

<!-- Plugins Js -->
<script src="assets/js/app.min.js"></script>
<script src="assets/js/chart.min.js"></script>
<!-- Custom Js -->
<script src="assets/js/admin.js"></script>
<script src="assets/js/pages/index.js"></script>
<script src="assets/js/pages/charts/jquery-knob.js"></script>
<script src="assets/js/pages/sparkline/sparkline-data.js"></script>
<script src="assets/js/pages/medias/carousel.js"></script>
</body>