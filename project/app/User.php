<?php

namespace App;

use App\Models\Admin;
use App\Models\AdminPermission;
use App\Models\Bookmark;
use App\Models\Capitalist;
use App\Models\Cart;
use App\Models\Gender;
use App\Models\Marketer;
use App\Models\Permission;
use App\Models\PollChoice;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\ProductComment;
use App\Models\ProductPoint;
use App\Models\ProductPollingUser;
use App\Models\ProductRepository;
use App\Models\Rebate;
use App\Models\Repository;
use App\Models\Role;
use App\Models\Seller;
use App\Models\Ticket;
use App\Models\UserNotice;
use App\Models\UserPollChoice;
use App\Models\UserProductQuestion;
use App\Models\UserProductQuestionAnswer;
use App\Models\UserRebate;
use App\Models\UserSuggestion;
use App\Models\UserVisitProduct;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'password','phone_number', 'email', 'birth_date','api_token','gender_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function gender(){
        return $this->belongsTo(Gender::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }

    public function admin(){
        return $this->hasOne(Admin::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'admin_permissions')->using(AdminPermission::class)->withTimestamps();
    }

    public function repositories(){
        return $this->hasMany(Repository::class,'owner_id');
    }

    public function capitalist(){
        return $this->hasOne(Capitalist::class);
    }

    public function marketer(){
        return $this->hasOne(Marketer::class);
    }

    public function seller(){
        return $this->hasOne(Seller::class);
    }

    public function tickets(){
        return $this->hasMany(Ticket::class);
    }

    public function pollChoices(){
        return $this->belongsToMany(PollChoice::class,'user_poll_choice','user_id','poll_choice_id')->using(UserPollChoice::class);
    }

    public function suggestions(){
        return $this->hasMany(UserSuggestion::class);
    }

    public function products(){
        return $this->hasMany(Product::class);
    }

    public function productsRepositories(){
        return $this->hasMany(ProductRepository::class,'owner_id');
    }

    public function productsRepositoriesGenerated(){
        return $this->hasMany(ProductRepository::class);
    }

    public function productsAttributesGenerated(){
        return $this->hasMany(ProductAttribute::class);
    }

    public function rebates()
    {
        return $this->belongsToMany(Rebate::class,'user_rebate')->using(UserRebate::class);
    }

    public function carts(){
        return $this->hasMany(Cart::class);
    }

    public function bookmarks()
    {
        return $this->hasMany(Bookmark::class);
    }

    public function productsPoint(){
        return $this->hasMany(ProductPoint::class);
    }

    public function productVisited()
    {
        return $this->belongsToMany(Product::class,'user_visited_products')->using(UserVisitProduct::class);
    }

    public function comments(){
        return $this->hasMany(ProductComment::class);
    }

    public function productNotices(){
        return $this->belongsToMany(Product::class,'user_notices')->using(UserNotice::class);
    }

    public function questions(){
        return $this->belongsToMany(Product::class,'product_user_questions')->using(UserProductQuestion::class);
    }

    public function answers()
    {
        return $this->hasMany(UserProductQuestionAnswer::class);
    }

    public function productPolling()
    {
        return $this->hasMany(ProductPollingUser::class);
    }
}
