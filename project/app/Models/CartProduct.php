<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class CartProduct extends Pivot
{
    protected $guarded =['id'];

    protected $table='cart_product';

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    public function product()
    {
        return $this->belongsTo(ProductRepository::class,'product_repository_id');
    }

    public function rebate()
    {
        return $this->belongsTo(Rebate::class);
    }

    public function seller()
    {
        return $this->belongsTo(Seller::class);
    }
}
