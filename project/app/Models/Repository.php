<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Repository extends Model
{
    protected $guarded =['id'];

    public function owner(){
        return $this->belongsTo(User::class,'owner_id');
    }

    public function province(){
        return $this->belongsTo(Province::class);
    }

    public function admin(){
        return $this->belongsTo(Admin::class,'admin_generated_id');
    }

    public function products()
    {
        return $this->belongsToMany(ProductAttribute::class,'product_repositories','repository_id','product_attribute_id')->using(ProductRepository::class);
    }
}
