<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GuestVisitProduct extends Model
{
    protected $guarded = ['id'];

    protected $table = 'guest_visited_products';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
