<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ProductPoint extends Model
{
    protected $guarded =['id'];

    protected $table='product_points';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
