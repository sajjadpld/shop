<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    protected $guarded = ['id'];

    public $timestamps = false;

    public function users(){
        return $this->hasMany(User::class);
    }

    public function categories(){
        return $this->hasMany(Category::class);
    }

    public function products(){
        return $this->hasMany(Product::class);
    }
}
