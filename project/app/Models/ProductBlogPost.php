<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductBlogPost extends Pivot
{
    protected $guarded =['id'];

    protected $table='blog_post_to_products';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function post()
    {
        return $this->belongsTo(BlogPost::class,'post_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
