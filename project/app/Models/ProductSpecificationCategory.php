<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSpecificationCategory extends Model
{
    protected $guarded =['id'];

    protected $table='product_specifications_category';

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
