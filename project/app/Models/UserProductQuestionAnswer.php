<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserProductQuestionAnswer extends Model
{
    protected $guarded = ['id'];

    protected $table = 'product_user_question_answers';

    public function question()
    {
        return $this->belongsTo(UserProductQuestion::class,'question_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
