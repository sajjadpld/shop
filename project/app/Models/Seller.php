<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $guarded = ['id'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function admin(){
        return $this->belongsTo(Admin::class,'admin_generated_id');
    }

    public function settings(){
        return $this->hasMany(SellerSetting::class);
    }

    public function cartProducts()
    {
        return $this->hasMany(CartProduct::class);
    }
}
