<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Relations\Pivot;

class AdminPermission extends Pivot
{
    protected $table = 'admin_permissions';

    public $incrementing = true;

    protected $guarded = ['id'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function permission(){
        return $this->belongsTo(Permission::class);
    }

    public function admin(){
        return $this->belongsTo(Admin::class,'admin_generated_id');
    }
}
