<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductPair extends Model
{
    protected $guarded =['id'];

    protected $table='pair_products';

    public function product()
    {
        return $this->belongsTo(ProductAttribute::class,'product_attribute_id');
    }

    public function pair()
    {
        return $this->belongsTo(ProductAttribute::class,'pair_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
