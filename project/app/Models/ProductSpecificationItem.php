<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductSpecificationItem extends Pivot
{
    protected $guarded =['id'];

    protected $table='product_specifications_items';

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function key()
    {
        return $this->belongsTo(ProductSpecificationKey::class,'key_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function productSpecifications()
    {
        return $this->belongsToMany(Product::class,'product_specifications','item_id','product_id')->using(ProductSpecification::class);
    }
}
