<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogPostCategory extends Model
{
    protected $guarded = ['id'];

    protected $table = 'blog_post_category';

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function post()
    {
        return $this->belongsTo(BlogPost::class, 'post_id');
    }

    public function category()
    {
        return $this->belongsTo(BlogCategory::class, 'category_id');
    }
}
