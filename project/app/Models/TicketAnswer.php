<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketAnswer extends Model
{
    protected $guarded = ['id'];

    protected $table = 'ticket_answers';

    public function question()
    {
        return $this->belongsTo(TicketQuestion::class,'question_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
