<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    protected $guarded = ['id'];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function choices(){
        return $this->hasMany(PollChoice::class);
    }

    public function products(){
        return $this->belongsToMany(Product::class,'product_polling')->using(ProductPolling::class);
    }
}
