<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class RobotCategory extends Pivot
{
    protected $guarded = ['id'];

    protected $table = 'category_robot';

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function robot()
    {
        return $this->belongsTo(Robot::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
