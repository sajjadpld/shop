<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Relations\Pivot;

class UserRebate extends Pivot
{
    protected $guarded =['id'];

    protected $table='user_rebate';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function rebate()
    {
        return $this->belongsTo(Rebate::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
