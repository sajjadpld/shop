<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CostHistory extends Model
{
    protected $guarded = ['id'];

    protected $table = 'costs_histories';

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
