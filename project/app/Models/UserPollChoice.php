<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserPollChoice extends Model
{
    protected $guarded = ['id'];

    protected $table = 'user_poll_choice';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function choice(){
        return $this->belongsTo(PollChoice::class,'poll_choice_id');
    }
}
