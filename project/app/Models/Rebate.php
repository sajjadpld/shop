<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Rebate extends Model
{
    protected $guarded = ['id'];

    public function admin(){
        return $this->belongsTo(Admin::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class,'product_rebate')->using(ProductRebate::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class,'user_rebate')->using(UserRebate::class);
    }

    public function carts()
    {
        return $this->hasMany(Cart::class);
    }

    public function cartProducts()
    {
        return $this->hasMany(CartProduct::class);
    }
}
