<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductDisadvantageAdvantage extends Model
{
    protected $guarded = ['id'];

    protected $table = 'product_disadvantages_advantages';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
