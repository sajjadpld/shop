<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $guarded =['id'];

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }
}
