<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAttributeList extends Model
{
    protected $guarded =['id'];

    protected $table='product_attribute_list';

    public function productAttribute()
    {
        return $this->belongsTo(ProductAttribute::class,'product_attribute_id');
    }

    public function key()
    {
        return $this->belongsTo(AttributeKey::class,'attribute_key_id');
    }

    public function value()
    {
        return $this->belongsTo(AttributeValue::class,'attribute_value_id');
    }
}
