<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserSuggestion extends Model
{
    protected $guarded = ['id'];

    protected $table = 'user_suggestions';

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
