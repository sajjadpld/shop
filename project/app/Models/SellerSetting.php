<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SellerSetting extends Model
{
    protected $guarded =['id'];

    protected $table='seller_settings';

    public function seller(){
        return $this->belongsTo(Seller::class);
    }
}
