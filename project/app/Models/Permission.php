<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $guarded = ['id'];

    public $timestamps = false;

    public function users()
    {
        return $this->belongsToMany(User::class,'admin_permissions')->using(AdminPermission::class)->withTimestamps();
    }
}
