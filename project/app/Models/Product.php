<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function productAttributes()
    {
        return $this->hasMany(ProductAttribute::class);
    }

    public function rebates()
    {
        return $this->belongsToMany(Rebate::class,'product_rebate')->using(ProductRebate::class);
    }

    public function bookmarks()
    {
        return $this->hasMany(Bookmark::class);
    }

    public function cotsHistories(){
        return $this->hasMany(CostHistory::class);
    }

    public function usersPoint(){
        return $this->hasMany(ProductPoint::class);
    }

    public function productSpecifications()
    {
        return $this->belongsToMany(ProductSpecificationItem::class,'product_specifications','product_id','item_id')->using(ProductSpecification::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class,'product_tags')->using(ProductTag::class);
    }

    public function usersVisitor()
    {
        return $this->belongsToMany(User::class,'user_visited_products')->using(UserVisitProduct::class);
    }

    public function categories(){
        return $this->belongsToMany(Category::class,'product_categories')->using(ProductCategory::class);
    }

    public function gallery(){
        return $this->hasMany(ProductGallery::class);
    }

    public function comments(){
        return $this->hasMany(ProductComment::class);
    }

    public function posts(){
        return $this->belongsToMany(BlogPost::class,'blog_post_to_products','product_id','post_id')->using(ProductBlogPost::class);
    }

    public function guestsVisitor()
    {
        return $this->hasMany(GuestVisitProduct::class);
    }

    public function advantagesDisadvantages()
    {
        return $this->hasMany(ProductDisadvantageAdvantage::class);
    }

    public function gifts()
    {
        return $this->hasMany(ProductGift::class);
    }

    public function productForGifts()
    {
        return $this->hasMany(ProductGift::class,'gift_id');
    }

    public function userNotices(){
        return $this->belongsToMany(User::class,'user_notices')->using(UserNotice::class);
    }

    public function questions(){
        return $this->belongsToMany(User::class,'product_user_questions')->using(UserProductQuestion::class);
    }

    public function polls(){
        return $this->belongsToMany(Poll::class,'product_polling')->using(ProductPolling::class);
    }
}
