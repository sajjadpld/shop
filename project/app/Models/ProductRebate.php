<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductRebate extends Pivot
{
    protected $guarded =['id'];

    protected $table='product_rebate';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function rebate()
    {
        return $this->belongsTo(Rebate::class);
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
