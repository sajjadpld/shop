<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ProductPollingUser extends Model
{
    protected $guarded =['id'];

    protected $table='user_product_polling';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function productPoll()
    {
        return $this->belongsTo(ProductPolling::class,'product_polling_id');
    }

    public function pollChoice()
    {
        return $this->belongsTo(PollChoice::class,'poll_choice_id');
    }
}
