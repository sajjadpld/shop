<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStep extends Model
{
    protected $guarded = ['id'];

    protected $table = 'order_steps';

    public function carts()
    {
        return $this->hasMany(OrderCart::class,'order_step_id');
    }
}
