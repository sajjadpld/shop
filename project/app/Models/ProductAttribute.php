<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ProductAttribute extends Model
{
    protected $guarded = ['id'];

    protected $table = 'product_attributes';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function productAttributesList()
    {
        return $this->hasMany(ProductAttributeList::class, 'product_attribute_id');
    }

    public function repositories()
    {
        return $this->belongsToMany(ProductRepository::class,'product_repositories','product_attribute_id','repository_id')->using(ProductRepository::class);
    }

    public function pair()
    {
        //TODO get paired product on current product of 2 below relations
        //$this->hasMany(ProductPair::class, 'product_attribute_id');
        //$this->hasMany(ProductPair::class, 'pair_id');
    }
}
