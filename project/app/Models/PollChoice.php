<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PollChoice extends Model
{
    protected $guarded = ['id'];

    protected $table = 'poll_choices';

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function poll()
    {
        return $this->belongsTo(Poll::class);
    }

    public function users(){
        return $this->belongsToMany(User::class,'user_poll_choice','poll_choice_id','user_id')->using(UserPollChoice::class);
    }

    public function usersProductPollingChoice()
    {
        return $this->hasMany(ProductPollingUser::class,'poll_choice_id');
    }
}
