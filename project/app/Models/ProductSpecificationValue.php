<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSpecificationValue extends Model
{
    protected $guarded =['id'];

    protected $table='product_specifications_values';

    public function key()
    {
        return $this->belongsTo(ProductSpecificationKey::class,'key_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function productSpecifications()
    {
        return $this->hasMany(ProductSpecification::class,'value_id');
    }
}
