<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartAddress extends Model
{
    protected $guarded =['id'];

    protected $table='carts_address';

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }
}
