<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductPolling extends Pivot
{
    protected $guarded =['id'];

    protected $table='product_polling';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function poll()
    {
        return $this->belongsTo(Poll::class);
    }

    public function usersPolling()
    {
        return $this->hasMany(ProductPollingUser::class,'product_polling_id');
    }
}
