<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductRepository extends Pivot
{
    protected $guarded =['id'];

    protected $table='product_repositories';

    public function productAttribute()
    {
        return $this->belongsTo(ProductAttribute::class,'product_attribute_id');
    }

    public function repository()
    {
        return $this->belongsTo(Repository::class);
    }

    public function owner()
    {
        return $this->belongsTo(User::class,'owner_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function carts(){
        return $this->belongsToMany(Cart::class,'cart_product','product_repository_id','cart_id')->using(CartProduct::class);
    }


}
