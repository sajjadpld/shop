<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductGallery extends Model
{
    protected $guarded =['id'];

    protected $table='products_gallery';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function key()
    {
        return $this->belongsTo(AttributeKey::class,'key_id');
    }

    public function value()
    {
        return $this->belongsTo(AttributeValue::class,'value_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
