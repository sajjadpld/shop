<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeValue extends Model
{
    protected $guarded = ['id'];

    protected $table = 'attribute_values';

    public function admin(){
        return $this->belongsTo(Admin::class);
    }

    public function key(){
        return $this->belongsTo(AttributeKey::class,'key_id');
    }

    public function productsAttributeList(){
        return $this->hasMany(ProductAttributeList::class,'attribute_value_id');
    }

    public function productsGalleries(){
        return $this->hasMany(ProductGallery::class,'value_id');
    }
}
