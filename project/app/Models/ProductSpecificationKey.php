<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSpecificationKey extends Model
{
    protected $guarded =['id'];

    protected $table='product_specifications_keys';

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function values()
    {
        return $this->hasMany(ProductSpecificationValue::class,'key_id');
    }

    public function categories(){
        return $this->belongsToMany(Category::class,'product_specifications_items','key_id','category_id')->using(ProductSpecificationItem::class);
    }
}
