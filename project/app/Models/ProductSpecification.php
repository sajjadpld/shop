<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductSpecification extends Pivot
{
    protected $guarded =['id'];

    protected $table='product_specifications';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function items()
    {
        return $this->belongsTo(ProductSpecificationItem::class,'item_id');
    }

    public function value()
    {
        return $this->belongsTo(ProductSpecificationValue::class,'value_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
