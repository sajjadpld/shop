<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
    protected $guarded = ['id'];

    protected $table = 'blog_posts';

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function categories(){
        return $this->belongsToMany(BlogCategory::class,'blog_post_category','post_id','category_id')->using(BlogPostCategory::class);
    }

    public function comments(){
        return $this->hasMany(BlogPostComment::class,'post_id');
    }

    public function products(){
        return $this->belongsToMany(Product::class,'blog_post_to_products','post_id','product_id')->using(ProductBlogPost::class);
    }
}
