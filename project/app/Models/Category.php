<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = ['id'];

    public function admin(){
        return $this->belongsTo(Admin::class);
    }

    public function gender(){
        return $this->belongsTo(Gender::class);
    }

    public function parent(){
        return $this->belongsTo(Category::class,'parent_id');
    }

    public function childes(){
        return $this->hasMany(Category::class,'parent_id');
    }

    public function robots(){
        return $this->belongsToMany(Robot::class,'category_robot')->using(RobotCategory::class);
    }

    public function productSpecifications()
    {
        return $this->hasMany(ProductSpecificationCategory::class);
    }

    public function keys(){
        return $this->belongsToMany(ProductSpecificationKey::class,'product_specifications_items','category_id','key_id')->using(ProductSpecificationItem::class);
    }

    public function products(){
        return $this->belongsToMany(Product::class,'product_categories')->using(ProductCategory::class);
    }

    public function newsLetter()
    {
        return $this->hasMany(NewsLetter::class);
    }
}
