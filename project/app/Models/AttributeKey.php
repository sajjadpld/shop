<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeKey extends Model
{
    protected $guarded = ['id'];

    protected $table = 'attribute_keys';

    public function admin(){
        return $this->belongsTo(Admin::class);
    }

    public function values(){
        return $this->hasMany(AttributeValue::class,'key_id');
    }

    public function productsAttributeList(){
        return $this->hasMany(ProductAttributeList::class,'attribute_key_id');
    }

    public function productsGalleries(){
        return $this->hasMany(ProductGallery::class,'key_id');
    }
}
