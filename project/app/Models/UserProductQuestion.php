<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Relations\Pivot;

class UserProductQuestion extends Pivot
{
    protected $guarded = ['id'];

    protected $table = 'product_user_questions';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function answers()
    {
        return $this->hasMany(UserProductQuestionAnswer::class,'question_id');
    }
}
