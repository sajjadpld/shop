<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $guarded = ['id'];

    public $timestamps = false;

    public function city(){
        return $this->belongsTo(City::class);
    }

    public function repositories(){
        return $this->hasMany(Repository::class);
    }

    public function cartsAddress(){
        return $this->hasMany(CartAddress::class);
    }
}
