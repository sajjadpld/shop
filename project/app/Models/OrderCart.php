<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderCart extends Model
{
    protected $guarded = ['id'];

    protected $table = 'order_carts';

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    public function step()
    {
        return $this->belongsTo(OrderStep::class,'order_step_id');
    }
}
