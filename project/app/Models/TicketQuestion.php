<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TicketQuestion extends Model
{
    protected $guarded = ['id'];

    protected $table = 'ticket_questions';

    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }

    public function answers()
    {
        return $this->hasMany(TicketAnswer::class,'question_id');
    }

}
