<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductGift extends Model
{
    protected $guarded =['id'];

    protected $table='product_gifts';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function gift()
    {
        return $this->belongsTo(Product::class,'gift_id');
    }

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }
}
