<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsLetter extends Model
{
    protected $guarded = ['id'];

    protected $table = 'news_letter';

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
