<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $guarded =['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function rebate()
    {
        return $this->belongsTo(Rebate::class);
    }

    public function products(){
        return $this->belongsToMany(ProductRepository::class,'cart_product','cart_id','product_repository_id')->using(CartProduct::class);
    }

    public function transaction()
    {
        return $this->hasOne(Transaction::class);
    }

    public function steps()
    {
        return $this->hasMany(OrderCart::class);
    }

    public function address()
    {
        return $this->hasOne(CartAddress::class);
    }
}
