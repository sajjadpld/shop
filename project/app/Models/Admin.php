<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $guarded =['id'];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function adminGenerated(){
        return $this->belongsTo(Admin::class,'admin_generated_id');
    }

    public function adminsGenerated(){
        return $this->hasMany(Admin::class,'admin_generated_id');
    }

    public function permissionsGenerated()
    {
        return $this->hasMany(AdminPermission::class,'admin_generated_id');
    }

    public function repositoriesGenerated(){
        return $this->hasMany(Repository::class,'admin_generated_id');
    }

    public function capitalistsGenerated(){
        return $this->hasMany(Capitalist::class,'admin_generated_id');
    }

    public function rebates(){
        return $this->hasMany(Rebate::class);
    }

    public function marketersGenerated(){
        return $this->hasMany(Marketer::class,'admin_generated_id');
    }

    public function sellersGenerated(){
        return $this->hasMany(Seller::class,'admin_generated_id');
    }

    public function categories(){
        return $this->hasMany(Category::class);
    }

    public function tags(){
        return $this->hasMany(Tag::class);
    }

    public function attributeKeys(){
        return $this->hasMany(AttributeKey::class);
    }

    public function attributeValues(){
        return $this->hasMany(AttributeValue::class);
    }

    public function notes(){
        return $this->hasMany(Note::class);
    }

    public function ticketAnswers(){
        return $this->hasMany(TicketAnswer::class);
    }

    public function blogPosts(){
        return $this->hasMany(BlogPost::class);
    }

    public function blogCategories(){
        return $this->hasMany(BlogCategory::class);
    }

    public function blogPostCategories(){
        return $this->hasMany(BlogPostCategory::class);
    }

    public function polls(){
        return $this->hasMany(Poll::class);
    }

    public function pollsChoices(){
        return $this->hasMany(PollChoice::class);
    }

    public function robots(){
        return $this->hasMany(Robot::class);
    }

    public function robotsCategories(){
        return $this->hasMany(RobotCategory::class);
    }

    public function brands(){
        return $this->hasMany(Brand::class);
    }

    public function productsRebate(){
        return $this->hasMany(ProductRebate::class);
    }

    public function usersRebate(){
        return $this->hasMany(UserRebate::class);
    }

    public function productsPair(){
        return $this->hasMany(ProductPair::class);
    }

    public function cotsHistories(){
        return $this->hasMany(CostHistory::class);
    }

    public function productSpecificationCategories()
    {
        return $this->hasMany(ProductSpecificationCategory::class);
    }

    public function productSpecificationKeys()
    {
        return $this->hasMany(ProductSpecificationKey::class);
    }

    public function productSpecificationValues()
    {
        return $this->hasMany(ProductSpecificationValue::class);
    }

    public function productSpecificationItems()
    {
        return $this->hasMany(ProductSpecificationItem::class);
    }

    public function productSpecifications()
    {
        return $this->hasMany(ProductSpecification::class);
    }

    public function productTags()
    {
        return $this->hasMany(ProductTag::class);
    }

    public function productCategories()
    {
        return $this->hasMany(ProductCategory::class);
    }

    public function productsGalleries(){
        return $this->hasMany(ProductGallery::class);
    }

    public function productBlogPosts(){
        return $this->hasMany(ProductBlogPost::class);
    }

    public function productAdvantagesDisadvantages(){
        return $this->hasMany(ProductDisadvantageAdvantage::class);
    }

    public function productsGifts(){
        return $this->hasMany(ProductGift::class);
    }
}
