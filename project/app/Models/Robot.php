<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Robot extends Model
{
    protected $guarded =['id'];

    public function admin(){
        return $this->belongsTo(Admin::class);
    }

    public function categories(){
        return $this->belongsToMany(Category::class,'category_robot')->using(RobotCategory::class);
    }
}
