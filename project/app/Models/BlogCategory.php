<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlogCategory extends Model
{
    protected $guarded = ['id'];

    protected $table = 'blog_categories';

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    public function parent()
    {
        return $this->belongsTo(BlogCategory::class, 'parent_id');
    }

    public function childes()
    {
        return $this->hasMany(BlogCategory::class, 'parent_id');
    }

    public function posts(){
        return $this->belongsToMany(BlogPost::class,'blog_post_category','category_id','post_id')->using(BlogPostCategory::class);
    }
}
